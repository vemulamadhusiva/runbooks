stages:
  - images
  - test
  - deploy
  - alertmanager
  - deploy-rules
  - deploy-rules-production

default:
  image: "${CI_REGISTRY_IMAGE}:latest"
  tags:
    - gitlab-org

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

.deploy-rules:
  extends: .rules-artifacts
  stage: deploy-rules
  script:
    - gcloud auth activate-service-account --key-file ${SERVICE_KEY}
    - gcloud config set project ${PROJECT}
    - gcloud container clusters get-credentials ${CLUSTER} --region ${REGION}
    - ./bin/delete_orphan_kubenetes_rules.sh # Delete ophaned PrometheusRules
    - kubectl apply --namespace monitoring --filename ${CI_PROJECT_DIR}/rules-k8s/
  only:
    refs:
      - master
    variables:
      - $CI_API_V4_URL == "https://ops.gitlab.net/api/v4"
  tags:
    - release

.rules-artifacts:
  artifacts:
    expire_in: 1 day
    paths:
      - rules-k8s

.dashboards:
  before_script:
    - ./scripts/bundler.sh # Install jsonnet bundles
    - dashboards/generate-mixins.sh # Generate dashboards from mixins
  only:
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/
    refs:
      - master
      - merge_requests
      - tags

verify:
  stage: test
  script:
    - make verify

test-rules:
  extends: .rules-artifacts
  stage: test
  script:
    - gem install bundler --no-document
    - bundle install --with=test
    - bundle exec ./bin/create_kubernetes_rules.rb --create --validate

danger:
  stage: test
  script:
    - gem install bundler --no-document
    - bundle install --with=test
    - bundle exec danger --fail-on-errors=true --verbose
  only:
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/
    refs:
      - merge_requests

rubocop:
  stage: test
  script:
    - gem install bundler --no-document
    - bundle install --with=test
    - bundle exec rubocop

rspec:
  stage: test
  script:
    - gem install bundler --no-document
    - bundle install --with=test
    - bundle exec rspec

deploy-rules-gstg:
  environment: gstg
  extends: .deploy-rules
  stage: deploy-rules

deploy-rules-pre:
  environment: pre
  extends: .deploy-rules
  stage: deploy-rules

deploy-rules-ops:
  environment: ops
  extends: .deploy-rules
  stage: deploy-rules

deploy-rules-production:
  environment: gprd
  extends: .deploy-rules
  stage: deploy-rules-production

update-alertmanager:
  stage: alertmanager
  environment: ops
  tags:
    - release
  script:
    - make test-alertmanager
    - cd alertmanager
    - ./update.sh
  only:
    refs:
      - master
    changes:
      - alertmanager/*
    variables:
      - $CI_API_V4_URL == "https://ops.gitlab.net/api/v4"

test:
  stage: test
  script:
    - make test

ensure_generated_content_up_to_date:
  stage: test
  script:
    - make generate
    - git diff --exit-code || (echo "Please run 'make generate'" && exit 1)


# log.gitlab.net
################################################################################
update_elastic_log_dashboards:
  stage: deploy
  script:
  - ./elastic/managed-objects/log/dashboards/update-dashboards.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log/dashboards/*
    variables:
      - $ES_LOG_URL

update_elastic_log_searches:
  stage: deploy
  script:
  - ./elastic/managed-objects/log/searches/update-searches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log/searches/*
    variables:
      - $ES_LOG_URL

update_elastic_log_watches:
  stage: deploy
  script:
  - ./elastic/managed-objects/log/watches/update-watches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log/watches/*
    variables:
      - $ES_LOG_URL

update_elastic_log_visualizations:
  stage: deploy
  script:
  - ./elastic/managed-objects/log/visualizations/update-visualizations.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log/visualizations/*
    variables:
      - $ES_LOG_URL

# log.gprd.gitlab.net
################################################################################
update_elastic_log_gprd_ilm:
  stage: deploy
  script:
  - ./elastic/managed-objects/log_gprd/ILM/update-ilm.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log_gprd/ILM/*
    variables:
      - $ES_LOG_GPRD_URL

update_elastic_log_gprd_watches:
  stage: deploy
  script:
  - ./elastic/managed-objects/log_gprd/watches/update-watches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log_gprd/watches/*
    variables:
      - $ES_LOG_GPRD_URL

update_elastic_log_gprd_index_templates:
  stage: deploy
  script:
  - ./elastic/managed-objects/log_gprd/index-templates/update-index-templates.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log_gprd/index-templates/*
      - elastic/managed-objects/lib/log_gprd_index_template.libsonnet
      - elastic/managed-objects/lib/index_mappings/*.jsonnet
    variables:
      - $ES_LOG_GPRD_URL

update_elastic_log_gprd_cluster_settings:
  stage: deploy
  script:
  - ./elastic/managed-objects/log_gprd/cluster-settings/update-cluster-settings.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/log_gprd/cluster-settings/*
    variables:
      - $ES_LOG_GPRD_URL

# nonprod-log.gitlab.net
################################################################################
update_elastic_nonprod-log_watches:
  stage: deploy
  script:
  - ./elastic/managed-objects/nonprod-log/watches/update-watches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/nonprod-log/watches/*
    variables:
      - $ES_NONPROD_URL

update_elastic_nonprod-log_ilm:
  stage: deploy
  script:
  - ./elastic/managed-objects/nonprod-log/ILM/update-ilm.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/nonprod-log/ILM/*
    variables:
      - $ES_NONPROD_URL

update_elastic_nonprod-log_index_templates:
  stage: deploy
  script:
  - ./elastic/managed-objects/nonprod-log/index-templates/update-index-templates.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/nonprod-log/index-templates/*
      - elastic/managed-objects/lib/nonprod-log_index_template.libsonnet
      - elastic/managed-objects/lib/index_mappings/*.libsonnet
    variables:
      - $ES_NONPROD_URL

update_elastic_nonprod-log_cluster_settings:
  stage: deploy
  script:
  - ./elastic/managed-objects/nonprod-log/cluster-settings/update-cluster-settings.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/nonprod-log/cluster-settings/*
    variables:
      - $ES_NONPROD_URL

# security-dev-20200423
################################################################################
update_elastic_security_dev_ilm:
  stage: deploy
  script:
  - ./elastic/managed-objects/security_dev/ILM/update-ilm.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/security_dev/ILM/*
    variables:
      - $ES_SECURITY_DEV_URL

update_elastic_security_dev_watches:
  stage: deploy
  script:
  - ./elastic/managed-objects/security_dev/watches/update-watches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/security_dev/watches/*
    variables:
      - $ES_SECURITY_DEV_URL

update_elastic_security_dev_index_templates:
  stage: deploy
  script:
  - ./elastic/managed-objects/security_dev/index-templates/update-index-templates.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/security_dev/index-templates/*
    variables:
      - $ES_SECURITY_DEV_URL

update_elastic_security_dev_cluster_settings:
  stage: deploy
  script:
  - ./elastic/managed-objects/security_dev/cluster-settings/update-cluster-settings.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/security_dev/cluster-settings/*
    variables:
      - $ES_SECURITY_DEV_URL

# monitoring-es7
################################################################################

update_elastic_monitoring-es7_cluster_settings:
  stage: deploy
  script:
  - ./elastic/managed-objects/monitoring-es7/cluster-settings/update-cluster-settings.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/monitoring-es7/cluster-settings/*
    variables:
      - $ES_MONITORING_ES7_URL

update_elastic_monitoring-es7_ilm:
  stage: deploy
  script:
  - ./elastic/managed-objects/monitoring-es7/ILM/update-ilm.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/monitoring-es7/ILM/*
    variables:
      - $ES_MONITORING_ES7_URL

################################################################################

dryrun_pingdom_checks:
  stage: test
  image: golang:1.11
  script:
    - cd pingdom
    - go run pingdom.go --dry-run
  except:
    refs:
      - master
  only:
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/
    refs:
      - merge_requests
      - tags

deploy_pingdom_checks:
  stage: deploy
  image: golang:1.11
  script:
    - cd pingdom
    - go run pingdom.go
  only:
    refs:
      - master
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/

check_alerts:
  image: golang:1.14
  script:
    - cd alerts-checker
    # TODO use go modules rather than fetching HEAD
    # We are seeing errors related to
    # github.com/prometheus/prometheus/promql/parser when we try to set up go
    # modules. For now, let's get this working hackily.
    - go get github.com/prometheus/prometheus/...
    - go run alerts-checker.go ../rules $THANOS_URL $IGNORED_ALERTS
  only:
    variables:
      - $PERFORM_ALERTS_CHECK
  tags:
    - release

test_dashboards:
  extends: .dashboards
  stage: test
  script:
    - dashboards/upload.sh -D

deploy_dashboards:
  extends: .dashboards
  stage: deploy
  script:
    - dashboards/upload.sh
    - dashboards/delete-orphaned-dashboards.sh
  only:
    refs:
      - master

.docker_image_template:
  image: docker:stable
  services:
    - docker:dind
  retry: 2
  variables:
    DOCKER_TLS_CERTDIR: ""
    IMAGE: ${CI_REGISTRY_IMAGE}

docker_image_test:
  extends: .docker_image_template
  stage: test
  script:
    - docker build .
  only:
    changes:
      - Dockerfile
    refs:
      - merge_requests
      - tags

docker_image_build:
  extends: .docker_image_template
  stage: images
  script:
    - export ci_image_tag=${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}
    - echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY
    - docker build -t ${IMAGE}:$ci_image_tag -t ${IMAGE}:latest .
    - docker push ${IMAGE}:latest
    - docker push ${IMAGE}:$ci_image_tag
  only:
    - tags

publish:
  image: node:10
  stage: images
  before_script:
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release
  only:
    refs:
      - master
  except:
    variables:
      # Publishing only happens on gitlab.com, the tag
      # will be created on gitlab.com, mirrored to ops,
      # and the image will be built in both locations
      - $CI_API_V4_URL == "https://ops.gitlab.net/api/v4"
    refs:
      - tags

alertmanager_test:
  stage: test
  script:
    - make test-alertmanager
  only:
    changes:
      - alertmanager/*
    refs:
      - master
      - merge_requests
      - tags
