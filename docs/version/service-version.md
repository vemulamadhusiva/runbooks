<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Version Service
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22version%22%2C%20tier%3D%22sv%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Version"

## Logging

* [production.log](/var/log/version/)

## Troubleshooting Pointers

* [../cloudflare/managing-traffic.md](../cloudflare/managing-traffic.md)
* [../cloudflare/services-locations.md](../cloudflare/services-locations.md)
* [../elastic/elastic-cloud.md](../elastic/elastic-cloud.md)
* [../gitaly/gitaly-error-rate.md](../gitaly/gitaly-error-rate.md)
* [../gitaly/storage-sharding.md](../gitaly/storage-sharding.md)
* [../logging/README.md](../logging/README.md)
* [../logging/logging_gcs_archive_bigquery.md](../logging/logging_gcs_archive_bigquery.md)
* [../monitoring/filesystem_alerts_inodes.md](../monitoring/filesystem_alerts_inodes.md)
* [../monitoring/update-prometheus-and-exporters.md](../monitoring/update-prometheus-and-exporters.md)
* [../packaging/manage-package-signing-keys.md](../packaging/manage-package-signing-keys.md)
* [../patroni/geo-patroni-cluster.md](../patroni/geo-patroni-cluster.md)
* [../patroni/patroni-management.md](../patroni/patroni-management.md)
* [../patroni/postgres-checkup.md](../patroni/postgres-checkup.md)
* [../patroni/postgres_exporter.md](../patroni/postgres_exporter.md)
* [../patroni/postgresql-backups-wale-walg.md](../patroni/postgresql-backups-wale-walg.md)
* [../pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md](../pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md)
* [../postgres-dr-delayed/postgres-dr-replicas.md](../postgres-dr-delayed/postgres-dr-replicas.md)
* [../praefect/praefect-read-only.md](../praefect/praefect-read-only.md)
* [../release.gitlab.net/README.md](../release.gitlab.net/README.md)
* [../runner/update-gitlab-runner-on-managers.md](../runner/update-gitlab-runner-on-managers.md)
* [../uncategorized/about-gitlab-com.md](../uncategorized/about-gitlab-com.md)
* [../uncategorized/aptly.md](../uncategorized/aptly.md)
* [../uncategorized/chef-documentation.md](../uncategorized/chef-documentation.md)
* [../uncategorized/chef-guidelines.md](../uncategorized/chef-guidelines.md)
* [../uncategorized/chef-vault.md](../uncategorized/chef-vault.md)
* [../uncategorized/chefspec.md](../uncategorized/chefspec.md)
* [../uncategorized/cloudsql-data-export.md](../uncategorized/cloudsql-data-export.md)
* [../uncategorized/dev-environment.md](../uncategorized/dev-environment.md)
* [../uncategorized/k8s-cluster-upgrade.md](../uncategorized/k8s-cluster-upgrade.md)
* [../uncategorized/k8s-gitlab-operations.md](../uncategorized/k8s-gitlab-operations.md)
* [../uncategorized/k8s-operations.md](../uncategorized/k8s-operations.md)
* [../uncategorized/k8s-plantuml-operations.md](../uncategorized/k8s-plantuml-operations.md)
* [../uncategorized/manage-chef.md](../uncategorized/manage-chef.md)
* [../uncategorized/manage-pacemaker.md](../uncategorized/manage-pacemaker.md)
* [../uncategorized/manage-workers.md](../uncategorized/manage-workers.md)
* [../uncategorized/mtail.md](../uncategorized/mtail.md)
* [../uncategorized/omnibus-package-updates.md](../uncategorized/omnibus-package-updates.md)
* [../uncategorized/project-export.md](../uncategorized/project-export.md)
* [../uncategorized/remove-kernels.md](../uncategorized/remove-kernels.md)
* [../uncategorized/tweeting-guidelines.md](../uncategorized/tweeting-guidelines.md)
* [../uncategorized/upgrade-camoproxy.md](../uncategorized/upgrade-camoproxy.md)
* [../uncategorized/upgrade-docker-machine.md](../uncategorized/upgrade-docker-machine.md)
* [../uncategorized/uptycs_osquery.md](../uncategorized/uptycs_osquery.md)
* [../uncategorized/yubikey.md](../uncategorized/yubikey.md)
* [gitaly-version-mismatch.md](gitaly-version-mismatch.md)
* [version-gitlab-com.md](version-gitlab-com.md)
* [../web/static-repository-objects-caching.md](../web/static-repository-objects-caching.md)
<!-- END_MARKER -->
